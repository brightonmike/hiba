# WATB Hiba Template

Hiba is a beautifully designed static site, ready to show off your business. Best of all - it's totally free!

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
npm install && bower install
```

Then:

`grunt build`

While you're working on your project, run:

`grunt watch`

And you're ready to go!

## Directory Structure

  * `scss/_settings.scss`: Foundation configuration settings go in here
  * `scss/app.scss`: Application styles go here
